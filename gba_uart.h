#ifndef _H_GBA_UART_H_
#define _H_GBA_UART_H_

#include <stdint.h>
#include <stdbool.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef bool(*gba_uart_send_t)(void *ctx, const void *data, uint32_t size);
typedef bool(*gba_uart_recv_t)(void *ctx, void *data, uint32_t size);

typedef struct gba_uart_data {
    void *ctx;
    gba_uart_send_t send;
    gba_uart_recv_t recv;
} gba_uart_data_t;

void gba_uart_init(gba_uart_data_t *gba_uart, gba_uart_send_t send, gba_uart_recv_t recv, void *ctx);
bool gba_uart_transaction(gba_uart_data_t *gba_uart, const void *send_data, uint32_t send_size, void *recv_data, uint32_t recv_size);

#ifdef __cplusplus
};
#endif

#endif
