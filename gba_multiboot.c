#include "gba_multiboot.h"

#include <string.h>

#include <stdio.h>

static const uint8_t header[] = {
0x2E, 0x00, 0x00, 0xEA, 0x24, 0xFF, 0xAE, 0x51, 0x69, 0x9A, 0xA2, 0x21, 0x3D, 0x84, 0x82, 0x0A, 
0x84, 0xE4, 0x09, 0xAD, 0x11, 0x24, 0x8B, 0x98, 0xC0, 0x81, 0x7F, 0x21, 0xA3, 0x52, 0xBE, 0x19, 
0x93, 0x09, 0xCE, 0x20, 0x10, 0x46, 0x4A, 0x4A, 0xF8, 0x27, 0x31, 0xEC, 0x58, 0xC7, 0xE8, 0x33, 
0x82, 0xE3, 0xCE, 0xBF, 0x85, 0xF4, 0xDF, 0x94, 0xCE, 0x4B, 0x09, 0xC1, 0x94, 0x56, 0x8A, 0xC0, 
0x13, 0x72, 0xA7, 0xFC, 0x9F, 0x84, 0x4D, 0x73, 0xA3, 0xCA, 0x9A, 0x61, 0x58, 0x97, 0xA3, 0x27, 
0xFC, 0x03, 0x98, 0x76, 0x23, 0x1D, 0xC7, 0x61, 0x03, 0x04, 0xAE, 0x56, 0xBF, 0x38, 0x84, 0x00, 
0x40, 0xA7, 0x0E, 0xFD, 0xFF, 0x52, 0xFE, 0x03, 0x6F, 0x95, 0x30, 0xF1, 0x97, 0xFB, 0xC0, 0x85, 
0x60, 0xD6, 0x80, 0x25, 0xA9, 0x63, 0xBE, 0x03, 0x01, 0x4E, 0x38, 0xE2, 0xF9, 0xA2, 0x34, 0xFF, 
0xBB, 0x3E, 0x03, 0x44, 0x78, 0x00, 0x90, 0xCB, 0x88, 0x11, 0x3A, 0x94, 0x65, 0xC0, 0x7C, 0x63, 
0x87, 0xF0, 0x3C, 0xAF, 0xD6, 0x25, 0xE4, 0x8B, 0x38, 0x0A, 0xAC, 0x72, 0x21, 0xD4, 0xF8, 0x07, 
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
0x30, 0x31, 0x96, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xF0, 0x00, 0x00};

void gbamb_init(gbamb_data_t *gbamb, gbamb_spi_transaction_t transaction, void *ctx)
{
    memset(gbamb, 0, sizeof(*gbamb));
    gbamb->ctx = ctx;
    gbamb->transaction = transaction;
}

static bool transaction(gbamb_data_t *gbamb, uint32_t data, uint32_t *response)
{
    uint8_t tx[4];
    tx[0] = (uint8_t)((data >> 24) & 0xFF);
    tx[1] = (uint8_t)((data >> 16) & 0xFF);
    tx[2] = (uint8_t)((data >> 8) & 0xFF);
    tx[3] = (uint8_t)(data & 0xFF);
    if (!gbamb->transaction(gbamb->ctx, tx, sizeof(tx)))
    {
        return false;
    }
    if ( response )
    {
        *response = (uint32_t)(tx[3]) | ((uint32_t)(tx[2]) << 8) | ((uint32_t)(tx[1]) << 16) | ((uint32_t)(tx[0]) << 24);
    }
    return true;
}

static bool send_handshake(gbamb_data_t *gbamb, uint32_t rv)
{
    // handshake_data 11h+client_data[1]+client_data[2]+client_data[3]
    uint32_t handshake_data = (((rv >> 16) + 0xf) & 0xff) | 0x00006400;
    printf("Sending Handshake: 0x%08x\n", handshake_data);
    if (!transaction(gbamb, handshake_data, &rv))
    {
        return false;
    }
    printf("0x%08x\n", rv);
    return true;
}

bool send_rom_header(gbamb_data_t *gbamb)
{
    printf("Sending ROM Header\n");
    uint32_t block;

    uint32_t rv = 0;

    // Send Header in blocks of two bytes
    for (uint32_t i = 0; i <= 0x5f; i++)
    {
        block = header[2 * i];
        block = header[2 * i + 1] << 8 | block;
        gbamb->fcnt += 2;

        if ( !transaction(gbamb, block, NULL) )
        {
            return false;
        }
        printf("0x%08x\n", block);
    }
    printf("Header Transfered\n");
    if ( !transaction(gbamb, 0x00006200, &rv) )
    {
        return false;
    }
    printf("0x%08x\n", rv);
    return true;
}

static uint32_t gbamb_read4(gbamb_data_t *gbamb, uint32_t offset, gbamb_rom_read_t rom_read, void *ctx)
{
    return (uint32_t)(rom_read(ctx, offset)) | ((uint32_t)(rom_read(ctx, offset + 1)) << 8) | ((uint32_t)(rom_read(ctx, offset + 2)) << 16) | ((uint32_t)(rom_read(ctx, offset + 3)) << 24);
}

bool gbamb_load(gbamb_data_t *gbamb, uint32_t rom_size, gbamb_rom_read_t rom_read, void *ctx)
{
    if ( !gbamb || !gbamb->transaction || !rom_size || !rom_read )
    {
        return false;
    }

    uint32_t w, w2, bitt;

    // skip the header (if present in the rom)
    uint32_t rom_offset = 0;
    w = gbamb_read4(gbamb, 0, rom_read, ctx);
    if ( 0xea00002e == w )
    {
        rom_offset += 0xc0;
    }

    gbamb->fcnt = 0;

    rom_size = sizeof(header) + rom_size;
    rom_size = (rom_size + 0xf) & 0xFFFFFFF0; // Align rom length to 16
    printf("ROM Size: 0x%x\n", rom_size);

    uint32_t rv;

    printf("Waiting for GBA\n");
    while (rv != 0x72026202)
    {
        if ( !transaction(gbamb, 0x00006202, &rv) )
        {
            return false;
        }
    }
    printf("GBA Found: 0x%08x\n", rv);
    rv = 0;

    if ( !transaction(gbamb, 0x00006202, NULL) ) // Found GBA
    {
        return false;
    }
    if ( !transaction(gbamb, 0x00006102, NULL) ) // Recognition OK
    {
        return false;
    }

    if ( !send_rom_header(gbamb) )  // Transfer C0h bytes header data in units of 16bits with no encrpytion
    {
        return false;
    }

    if ( !transaction(gbamb, 0x00006202, &rv) ) // Exchange master/slave info again
    {
        return false;
    }
    printf("0x%08x\n", rv);

    printf("Sending Palette\n");
    // palette_data as "81h+color*10h+direction*8+speed*2", or as "0f1h+color*2" for fixed palette, whereas color=0..6, speed=0..3, direction=0..1.
    // Then wait until 0x73hh**** is received. hh represents client_data
    while (((rv >> 24) & 0xFF) != 0x73)
    {
        if ( !transaction(gbamb, 0x000063D1, &rv) )
        {
            return false;
        }
        printf("0x%08x\n", rv);
    }

    uint32_t client_data = ((rv >> 16) & 0xFF); // Random client generated data used for later handshake
    printf("Client Data:\n");
    printf("0x%08x\n", client_data);

    uint32_t m = ((rv & 0x00ff0000) >>  8) + 0xffff00d1;
    uint32_t h = ((rv & 0x00ff0000) >> 16) + 0xf;

    if ( !send_handshake(gbamb, rv) )
    {
        return false;
    }

    printf("Sending length information: 0x%08x\n", (rom_size - 0x190) / 4);
    if ( !transaction(gbamb, (rom_size - 0x190) / 4, &rv) ) // Send length information and receive random data[1-3] (seed)
    {
        return false;
    }
    printf("0x%08x\n", rv);

    uint32_t f = (((rv & 0x00ff0000) >> 8) + h) | 0xffff0000;
    uint32_t c = 0x0000c387;

    uint32_t bytes_sent = 0;
    int i = 0;

    printf("Sending ROM\n");
    while (gbamb->fcnt < rom_size)
    {
        if (bytes_sent == 32)
        {
            bytes_sent = 0;
        }

        w = gbamb_read4(gbamb, i + rom_offset, rom_read, ctx);

        i = i + 4;
        bytes_sent += 4;

        if (gbamb->fcnt % 0x100 == 0 || gbamb->fcnt == rom_size) {
            printf("0x%08x / 0x%08x\n", gbamb->fcnt, rom_size);
        }

        w2 = w;

        for (bitt = 0; bitt < 32; bitt++)
        {
            if ((c ^ w) & 0x01) {
                c = (c >> 1) ^ 0x0000c37b;
            }
            else {
                c = c >> 1;
            }
            w = w >> 1;
        }

        m = (0x6f646573 * m) + 1;

        if ( !transaction(gbamb, w2 ^ ((~(0x02000000 + gbamb->fcnt)) + 1) ^m ^ 0x43202f2f, &rv) )
        {
            return false;
        }

        gbamb->fcnt = gbamb->fcnt + 4;
    }
    printf("0x%08x\n", rv);
    printf("ROM sent! Doing checksum now...\n");

    for (bitt = 0; bitt < 32; bitt++)
    {
        if ((c ^ f) & 0x01) {
        c = ( c >> 1) ^ 0x0000c37b;
        }
        else {
        c = c >> 1;
        }

        f = f >> 1;
    }
    // Serial.print("CRC: "); Serial.println(c, HEX);

    printf("Waiting for CRC\n");
    while (rv != 0x00750065) {
        if ( !transaction(gbamb, 0x00000065, &rv) )
        {
            return false;
        }
    }

    if ( !transaction(gbamb, 0x00000066, &rv) )
    {
        return false;
    }
    printf("0x%08x\n", rv);

    printf("Exchanging CRC\n");
    if ( !transaction(gbamb, c, &rv) )
    {
        return false;
    }
    printf("0x%08x\n", rv);

    printf("Done!\n");

    return true;
}
