#include <Adafruit_GBA.h>
#include <Adafruit_GBA_ROM.h>

Adafruit_GBA tft;

#ifndef ESP32
#error ADAPTER_CODE_NOT_YET_IMPLEMENTED_FOR_PLATFORM
#endif

#ifdef ESP32

#include "driver/uart.h"

// Note: UART2 default pins IO16, IO17 do not work on ESP32-WROVER module
// because these pins connected to PSRAM
#define ECHO_TEST_TXD   (23)
#define ECHO_TEST_RXD   (25)

// RTS for RS485 Half-Duplex Mode manages DE/~RE
#define ECHO_TEST_RTS   (18)

// CTS is not used in RS485 Half-Duplex Mode
#define ECHO_TEST_CTS  UART_PIN_NO_CHANGE

#define BUF_SIZE        (0xff)
#define BAUD_RATE       (115200)

// Read packet timeout
#define PACKET_READ_TICS        (10 / portTICK_RATE_MS)
#define ECHO_TASK_STACK_SIZE    (2048)
#define ECHO_TASK_PRIO          (10)
#define UART_PORT          (UART_NUM_2)

// CTS is not used in RS485 Half-Duplex Mode
#define ECHO_TEST_CTS  UART_PIN_NO_CHANGE

void esp32_gba_uart_init() {
    uart_config_t uart_config = {
            .baud_rate = BAUD_RATE,
            .data_bits = UART_DATA_8_BITS,
            .parity = UART_PARITY_DISABLE,
            .stop_bits = UART_STOP_BITS_1,
            .flow_ctrl = UART_HW_FLOWCTRL_DISABLE
    };

    uart_param_config(UART_PORT, &uart_config);
    uart_set_pin(UART_PORT, ECHO_TEST_TXD, ECHO_TEST_RXD, ECHO_TEST_RTS, ECHO_TEST_CTS);
    uart_driver_install(UART_PORT, BUF_SIZE * 2, 0, 0, NULL, 0);
    uart_set_mode(UART_PORT, UART_MODE_RS485_HALF_DUPLEX);
}

bool esp32_gba_uart_send(void *ctx, const void *data, uint32_t size)
{
    uint32_t sent = 0;
    int len = 0;
    while ( sent < size )
    {
        len = uart_write_bytes(UART_PORT, ((const char *)data) + sent, size - sent);
        if ( len > 0 )
        {
            sent += (uint32_t)len;
        }
    }
    return true;
}

bool esp32_gba_uart_recv(void *ctx, void *data, uint32_t size)
{
    uint32_t recd = 0;
    int len = 0;
    while ( recd < size )
    {
        len = uart_read_bytes(UART_PORT, ((uint8_t *)data) + recd, size - recd, PACKET_READ_TICS);
        if ( len > 0 )
        {
            recd += (uint32_t)len;
        }
    }
    return true;
}

#endif  // ESP32

#ifdef ESP32

#define ESP32_GBA_SPI_PIN_MISO 25
#define ESP32_GBA_SPI_PIN_MOSI 23
#define ESP32_GBA_SPI_PIN_CLK  19
#define ESP32_GBA_SPI_PIN_CS   22

#include <esp_err.h>
#include <driver/spi_master.h>
#include <driver/gpio.h>

spi_device_handle_t esp32_gba_spi;

bool esp32_gba_spi_transaction(void *ctx, void *data, uint32_t size)
{
    esp_err_t ret;
    spi_transaction_t trans;
    memset(&trans, 0, sizeof(spi_transaction_t));
    trans.length = 8 * size;
    trans.user = (void *) 1;
    trans.tx_buffer = (uint8_t*)data;
    trans.flags = SPI_TRANS_USE_RXDATA;
    ret = spi_device_transmit(esp32_gba_spi, &trans);
    memcpy(data, trans.rx_data, size);
    delayMicroseconds(36);
    ESP_ERROR_CHECK(ret);
    return ESP_OK == ret;
}

void esp32_gba_spi_init()
{
    esp_err_t ret;
    spi_bus_config_t buscfg;
    memset(&buscfg, 0, sizeof(buscfg));
    buscfg.miso_io_num=ESP32_GBA_SPI_PIN_MISO;
    buscfg.mosi_io_num=ESP32_GBA_SPI_PIN_MOSI;
    buscfg.sclk_io_num=ESP32_GBA_SPI_PIN_CLK;
    buscfg.quadwp_io_num=-1;
    buscfg.quadhd_io_num=-1;
    buscfg.max_transfer_sz=8;
    spi_device_interface_config_t devcfg;
    memset(&devcfg, 0, sizeof(devcfg));
    devcfg.clock_speed_hz=5 * 100 * 1000;
    devcfg.mode=3;
    devcfg.spics_io_num=ESP32_GBA_SPI_PIN_CS;
    devcfg.queue_size=7;
    ret = spi_bus_initialize(HSPI_HOST, &buscfg, 1);
    ESP_ERROR_CHECK(ret);
    ret = spi_bus_add_device(HSPI_HOST, &devcfg, &esp32_gba_spi);
    ESP_ERROR_CHECK(ret);
    delayMicroseconds(10);
}

void esp32_gba_spi_fini()
{
    spi_bus_remove_device(esp32_gba_spi);
    spi_bus_free(HSPI_HOST);
}

#endif   // ESP32

void setup()
{
    Serial.begin(115200);
    Serial.println("BOOTING...");
#ifdef ESP32
    esp32_gba_spi_init();
    tft.load(esp32_gba_spi_transaction, sizeof(Adafruit_GBA_ROM_gba), Adafruit_GBA_ROM_gba);
    esp32_gba_spi_fini();
    esp32_gba_uart_init();
    tft.begin(esp32_gba_uart_send, esp32_gba_uart_recv);
#endif
    tft.set_button_callback([](uint16_t buttons){
        Serial.println(buttons, HEX);
    });
}

unsigned long testFillScreen() {
  unsigned long start = micros();
  tft.fillScreen(GBA_GFX_COLOR_BLACK);
  yield();
  tft.fillScreen(GBA_GFX_COLOR_RED);
  yield();
  tft.fillScreen(GBA_GFX_COLOR_GREEN);
  yield();
  tft.fillScreen(GBA_GFX_COLOR_BLUE);
  yield();
  tft.fillScreen(GBA_GFX_COLOR_BLACK);
  yield();
  return micros() - start;
}

unsigned long testText() {
  tft.fillScreen(GBA_GFX_COLOR_BLACK);
  unsigned long start = micros();
  tft.setCursor(0, 0);
  tft.setTextColor(GBA_GFX_COLOR_WHITE);  tft.setTextSize(1);
  tft.println("Hello World!");
  tft.setTextColor(GBA_GFX_COLOR_YELLOW); tft.setTextSize(2);
  tft.println(1234.56);
  tft.setTextColor(GBA_GFX_COLOR_RED);    tft.setTextSize(3);
  tft.println(0xDEADBEEF, HEX);
  tft.println();
  tft.setTextColor(GBA_GFX_COLOR_GREEN);
  tft.setTextSize(5);
  tft.println("Groop");
  tft.setTextSize(2);
  tft.println("I implore thee,");
  tft.setTextSize(1);
  tft.println("my foonting turlingdromes.");
  tft.println("And hooptiously drangle me");
  tft.println("with crinkly bindlewurdles,");
  tft.println("Or I will rend thee");
  tft.println("in the gobberwarts");
  tft.println("with my blurglecruncheon,");
  tft.println("see if I don't!");
  return micros() - start;
}

unsigned long testLines(uint16_t color) {
  unsigned long start, t;
  int           x1, y1, x2, y2,
                w = tft.width(),
                h = tft.height();

  tft.fillScreen(GBA_GFX_COLOR_BLACK);
  yield();
  
  x1 = y1 = 0;
  y2    = h - 1;
  start = micros();
  for(x2=0; x2<w; x2+=6) tft.drawLine(x1, y1, x2, y2, color);
  x2    = w - 1;
  for(y2=0; y2<h; y2+=6) tft.drawLine(x1, y1, x2, y2, color);
  t     = micros() - start; // fillScreen doesn't count against timing

  yield();
  tft.fillScreen(GBA_GFX_COLOR_BLACK);
  yield();

  x1    = w - 1;
  y1    = 0;
  y2    = h - 1;
  start = micros();
  for(x2=0; x2<w; x2+=6) tft.drawLine(x1, y1, x2, y2, color);
  x2    = 0;
  for(y2=0; y2<h; y2+=6) tft.drawLine(x1, y1, x2, y2, color);
  t    += micros() - start;

  yield();
  tft.fillScreen(GBA_GFX_COLOR_BLACK);
  yield();

  x1    = 0;
  y1    = h - 1;
  y2    = 0;
  start = micros();
  for(x2=0; x2<w; x2+=6) tft.drawLine(x1, y1, x2, y2, color);
  x2    = w - 1;
  for(y2=0; y2<h; y2+=6) tft.drawLine(x1, y1, x2, y2, color);
  t    += micros() - start;

  yield();
  tft.fillScreen(GBA_GFX_COLOR_BLACK);
  yield();

  x1    = w - 1;
  y1    = h - 1;
  y2    = 0;
  start = micros();
  for(x2=0; x2<w; x2+=6) tft.drawLine(x1, y1, x2, y2, color);
  x2    = 0;
  for(y2=0; y2<h; y2+=6) tft.drawLine(x1, y1, x2, y2, color);

  yield();
  return micros() - start;
}

unsigned long testFastLines(uint16_t color1, uint16_t color2) {
  unsigned long start;
  int           x, y, w = tft.width(), h = tft.height();

  tft.fillScreen(GBA_GFX_COLOR_BLACK);
  start = micros();
  for(y=0; y<h; y+=5) tft.drawFastHLine(0, y, w, color1);
  for(x=0; x<w; x+=5) tft.drawFastVLine(x, 0, h, color2);

  return micros() - start;
}

unsigned long testRects(uint16_t color) {
  unsigned long start;
  int           n, i, i2,
                cx = tft.width()  / 2,
                cy = tft.height() / 2;

  tft.fillScreen(GBA_GFX_COLOR_BLACK);
  n     = min(tft.width(), tft.height());
  start = micros();
  for(i=2; i<n; i+=6) {
    i2 = i / 2;
    tft.drawRect(cx-i2, cy-i2, i, i, color);
  }

  return micros() - start;
}

unsigned long testFilledRects(uint16_t color1, uint16_t color2) {
  unsigned long start, t = 0;
  int           n, i, i2,
                cx = tft.width()  / 2 - 1,
                cy = tft.height() / 2 - 1;

  tft.fillScreen(GBA_GFX_COLOR_BLACK);
  n = min(tft.width(), tft.height());
  for(i=n; i>0; i-=6) {
    i2    = i / 2;
    start = micros();
    tft.fillRect(cx-i2, cy-i2, i, i, color1);
    t    += micros() - start;
    // Outlines are not included in timing results
    tft.drawRect(cx-i2, cy-i2, i, i, color2);
    yield();
  }

  return t;
}

unsigned long testFilledCircles(uint8_t radius, uint16_t color) {
  unsigned long start;
  int x, y, w = tft.width(), h = tft.height(), r2 = radius * 2;

  tft.fillScreen(GBA_GFX_COLOR_BLACK);
  start = micros();
  for(x=radius; x<w; x+=r2) {
    for(y=radius; y<h; y+=r2) {
      tft.fillCircle(x, y, radius, color);
    }
  }

  return micros() - start;
}

unsigned long testCircles(uint8_t radius, uint16_t color) {
  unsigned long start;
  int           x, y, r2 = radius * 2,
                w = tft.width()  + radius,
                h = tft.height() + radius;

  // Screen is not cleared for this one -- this is
  // intentional and does not affect the reported time.
  start = micros();
  for(x=0; x<w; x+=r2) {
    for(y=0; y<h; y+=r2) {
      tft.drawCircle(x, y, radius, color);
    }
  }

  return micros() - start;
}

unsigned long testTriangles() {
  unsigned long start;
  int           n, i, cx = tft.width()  / 2 - 1,
                      cy = tft.height() / 2 - 1;

  tft.fillScreen(GBA_GFX_COLOR_BLACK);
  n     = min(cx, cy);
  start = micros();
  for(i=0; i<n; i+=5) {
    tft.drawTriangle(
      cx    , cy - i, // peak
      cx - i, cy + i, // bottom left
      cx + i, cy + i, // bottom right
      tft.color565(i, i, i));
  }

  return micros() - start;
}

unsigned long testFilledTriangles() {
  unsigned long start, t = 0;
  int           i, cx = tft.width()  / 2 - 1,
                   cy = tft.height() / 2 - 1;

  tft.fillScreen(GBA_GFX_COLOR_BLACK);
  start = micros();
  for(i=min(cx,cy); i>10; i-=5) {
    start = micros();
    tft.fillTriangle(cx, cy - i, cx - i, cy + i, cx + i, cy + i,
      tft.color565(0, i*10, i*10));
    t += micros() - start;
    tft.drawTriangle(cx, cy - i, cx - i, cy + i, cx + i, cy + i,
      tft.color565(i*10, i*10, 0));
    yield();
  }

  return t;
}

/*
unsigned long testRoundRects() {
  unsigned long start;
  int           w, i, i2,
                cx = tft.width()  / 2 - 1,
                cy = tft.height() / 2 - 1;

  tft.fillScreen(GBA_GFX_COLOR_BLACK);
  w     = min(tft.width(), tft.height());
  start = micros();
  for(i=0; i<w; i+=6) {
    i2 = i / 2;
    tft.drawRoundRect(cx-i2, cy-i2, i, i, i/8, tft.color565(i, 0, 0));
  }

  return micros() - start;
}
*/

/*
unsigned long testFilledRoundRects() {
  unsigned long start;
  int           i, i2,
                cx = tft.width()  / 2 - 1,
                cy = tft.height() / 2 - 1;

  tft.fillScreen(GBA_GFX_COLOR_BLACK);
  start = micros();
  for(i=min(tft.width(), tft.height()); i>20; i-=6) {
    i2 = i / 2;
    tft.fillRoundRect(cx-i2, cy-i2, i, i, i/8, tft.color565(0, i, 0));
    yield();
  }

  return micros() - start;
}
*/

void loop()
{
  Serial.println("Graphics Test!");

  Serial.println(F("Benchmark                Time (microseconds)"));

  delay(10);
  Serial.print(F("Screen fill              "));
  Serial.println(testFillScreen());
  for ( uint8_t i = 0; i < 50; i++ ) { delay(10); tft.poll(); }

  Serial.print(F("Text                     "));
  Serial.println(testText());
  for ( uint16_t j = 0; j < 300; j++ ) { delay(10); tft.poll(); }

  Serial.print(F("Lines                    "));
  Serial.println(testLines(GBA_GFX_COLOR_CYAN));
  for ( uint8_t i = 0; i < 50; i++ ) { delay(10); tft.poll(); }

  Serial.print(F("Horiz/Vert Lines         "));
  Serial.println(testFastLines(GBA_GFX_COLOR_RED, GBA_GFX_COLOR_BLUE));
  for ( uint8_t i = 0; i < 50; i++ ) { delay(10); tft.poll(); }

  Serial.print(F("Rectangles (outline)     "));
  Serial.println(testRects(GBA_GFX_COLOR_GREEN));
  for ( uint8_t i = 0; i < 50; i++ ) { delay(10); tft.poll(); }

  Serial.print(F("Rectangles (filled)      "));
  Serial.println(testFilledRects(GBA_GFX_COLOR_YELLOW, GBA_GFX_COLOR_MAGENTA));
  for ( uint8_t i = 0; i < 50; i++ ) { delay(10); tft.poll(); }

  Serial.print(F("Circles (filled)         "));
  Serial.println(testFilledCircles(10, GBA_GFX_COLOR_MAGENTA));

  Serial.print(F("Circles (outline)        "));
  Serial.println(testCircles(10, GBA_GFX_COLOR_WHITE));
  for ( uint8_t i = 0; i < 50; i++ ) { delay(10); tft.poll(); }

  Serial.print(F("Triangles (outline)      "));
  Serial.println(testTriangles());
  for ( uint8_t i = 0; i < 50; i++ ) { delay(10); tft.poll(); }

  Serial.print(F("Triangles (filled)       "));
  Serial.println(testFilledTriangles());
  for ( uint8_t i = 0; i < 50; i++ ) { delay(10); tft.poll(); }

  //Serial.print(F("Rounded rects (outline)  "));
  //Serial.println(testRoundRects());
  //for ( uint8_t i = 0; i < 50; i++ ) { delay(10); tft.poll(); }

  //Serial.print(F("Rounded rects (filled)   "));
  //Serial.println(testFilledRoundRects());
  //for ( uint8_t i = 0; i < 50; i++ ) { delay(10); tft.poll(); }

  Serial.println(F("Done!"));
}
