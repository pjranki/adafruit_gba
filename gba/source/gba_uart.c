#include <gba.h>
#include <stdio.h>
#include <string.h>
#include "gba_uart.h"

#define SIO_CTS           0x0004
#define SIO_PARITY_ODD    0x0008
#define SIO_SEND_DATA     0x0010
#define SIO_RECV_DATA     0x0020
#define SIO_ERROR         0x0040
#define SIO_LENGTH_8      0x0080
#define SIO_USE_FIFO      0x0100
#define SIO_USE_PARITY    0x0200
#define SIO_SEND_ENABLE   0x0400
#define SIO_RECV_ENABLE   0x0800

#define GBA_UART_STATE_READY        0
#define GBA_UART_STATE_RECV_SIZE    1
#define GBA_UART_STATE_SEND_REPLY   2
#define GBA_UART_STATE_SENT_REPLY   3

void gba_uart_init(gba_uart_data_t *gba_uart, gba_uart_callback_t callback, void *ctx)
{
    REG_RCNT = R_UART;
    REG_SIOCNT = 0;
    REG_SIOCNT = SIO_115200 | SIO_LENGTH_8 | SIO_SEND_ENABLE | SIO_RECV_ENABLE | SIO_USE_FIFO | SIO_UART;
    memset(gba_uart, 0, sizeof(*gba_uart));
    gba_uart->callback = callback;
    gba_uart->ctx = ctx;
    gba_uart->state = GBA_UART_STATE_READY;
}

static void gba_uart_write_byte_block(uint8_t data)
{
    while (REG_SIOCNT & 0x0010) { /* wait forever */ }
    REG_SIODATA8 = data;
}

static bool gba_uart_read_byte(uint8_t *data)
{
    if (REG_SIOCNT & 0x0020)
    {
        return false;
    }

    *data = (unsigned char) REG_SIODATA8;
    return true;
}

static uint8_t gba_uart_read_byte_block()
{
    while (REG_SIOCNT & 0x0020) { /* wait forever */ }
    uint8_t data = (uint8_t) REG_SIODATA8;
    return data;
}

static void gba_uart_reply(void *reply_ctx, const void *send_data, uint32_t send_size)
{
    gba_uart_data_t *gba_uart = (gba_uart_data_t *)reply_ctx;
    if ( gba_uart->state != GBA_UART_STATE_SEND_REPLY )
    {
        return;
    }
    if ( send_size > 0xff )
    {
        return;
    }
    gba_uart_write_byte_block(0x0d);
    gba_uart_write_byte_block((uint8_t)(send_size & 0xff));
    for ( uint32_t i = 0; i < send_size; i++ )
    {
        gba_uart_write_byte_block(((const uint8_t *)send_data)[i]);
    }
    gba_uart->state = GBA_UART_STATE_SENT_REPLY;
}

void gba_uart_poll(gba_uart_data_t *gba_uart)
{
    uint8_t data[0xff];
    if ( gba_uart->state == GBA_UART_STATE_READY )
    {
        if ( gba_uart_read_byte( &data[0] ) )
        {
            if ( data[0] == 0x0d )
            {
                gba_uart->state = GBA_UART_STATE_RECV_SIZE;
            }
        }
    }
    if ( gba_uart->state != GBA_UART_STATE_RECV_SIZE )
    {
        gba_uart->state = GBA_UART_STATE_READY;
        return;
    }
    uint32_t recv_size = (uint32_t)gba_uart_read_byte_block();
    if ( recv_size > 0xff )
    {
        return;
    }
    for ( uint32_t i = 0; i < recv_size; i++ )
    {
        data[i] = gba_uart_read_byte_block();
    }
    gba_uart->state = GBA_UART_STATE_SEND_REPLY;
    gba_uart->callback(gba_uart->ctx, (const void *)data, recv_size, gba_uart_reply, (void*)gba_uart);
    if ( GBA_UART_STATE_SENT_REPLY != gba_uart->state )
    {
        gba_uart_write_byte_block(0x0d);
        gba_uart_write_byte_block(0x00);
    }
    gba_uart->state = GBA_UART_STATE_READY;
}
