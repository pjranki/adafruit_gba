#ifndef _H_GBA_GFX_H_
#define _H_GBA_GFX_H_

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

#define GBA_GFX_SCREEN_WIDTH    (240)
#define GBA_GFX_SCREEN_HEIGHT   (160)

void gba_gfx_init();
void gba_gfx_draw_pixel(int16_t x, int16_t y, uint16_t color);
void gba_gfx_draw_fast_v_line(int16_t x, int16_t y, int16_t h, uint16_t color);
void gba_gfx_draw_fast_h_line(int16_t x, int16_t y, int16_t w, uint16_t color);
void gba_gfx_fill_rect(int16_t x, int16_t y, int16_t w, int16_t h, uint16_t color);
void gba_gfx_fill_screen(uint16_t color);
void gba_gfx_draw_line(int16_t x0, int16_t y0, int16_t x1, int16_t y1, uint16_t color);
void gba_gfx_draw_rect(int16_t x, int16_t y, int16_t w, int16_t h, uint16_t color);

#define GBA_GFX_ENCODE_COLOR(color)             (((color>>(6+5))&0x001f)|(color&0x07e0)|((color<<(6+5))&0xf800))
void gba_gfx_draw_pixel_at_offset(uint16_t offset, uint16_t encoded_color);

#ifdef __cplusplus
};
#endif

#endif // _H_GBA_GFX_H_
