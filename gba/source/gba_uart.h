#ifndef _H_GBA_UART_H_
#define _H_GBA_UART_H_

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef void(*gba_uart_reply_t)(void *reply_ctx, const void *send_data, uint32_t send_size);
typedef void(*gba_uart_callback_t)(void *ctx, const void *recd_data, uint32_t recd_size, gba_uart_reply_t reply, void *reply_ctx);

typedef struct gba_uart_data {
    void *ctx;
    gba_uart_callback_t callback;
    uint32_t state;
} gba_uart_data_t;

void gba_uart_init(gba_uart_data_t *gba_uart, gba_uart_callback_t callback, void *ctx);
void gba_uart_poll(gba_uart_data_t *gba_uart);

#ifdef __cplusplus
};
#endif

#endif // _H_GBA_UART_H_
