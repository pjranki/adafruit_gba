#include <gba_console.h>
#include <gba_interrupt.h>
#include <gba_systemcalls.h>
#include <gba_input.h>
#include <stdio.h>
#include <gba.h>
#include "gba_gfx.h"
#include "gba_uart.h"

gba_uart_data_t gba_uart_data;

static uint32_t color = 0;

#define GET_BYTE_AS(data,offset,type)       ((type)(((const uint8_t*)(data))[(offset)]))
#define NTOH_INT8(data,offset)              (GET_BYTE_AS(data,offset,int8_t))
#define NTOH_UINT8(data,offset)             (GET_BYTE_AS(data,offset,uint8_t))
#define NTOH_INT16(data,offset)             ((GET_BYTE_AS(data,offset,int16_t)<<8)|(GET_BYTE_AS(data,offset+1,int16_t)<<0))
#define NTOH_UINT16(data,offset)            ((GET_BYTE_AS(data,offset,uint16_t)<<8)|(GET_BYTE_AS(data,offset+1,uint16_t)<<0))

#define GBA_COMMAND_RESERVED            0x00
#define GBA_COMMAND_DRAW_PIXEL          0x01
#define GBA_COMMAND_DRAW_FAST_VLINE     0x02
#define GBA_COMMAND_DRAW_FAST_HLINE     0x03
#define GBA_COMMAND_FILL_RECT           0x04
#define GBA_COMMAND_FILL_SCREEN         0x05
#define GBA_COMMAND_DRAW_LINE           0x06
#define GBA_COMMAND_DRAW_RECT           0x07
#define GBA_COMMAND_OPTIMISED_1         0x08
#define GBA_COMMAND_NOP                 0x09
#define GBA_COMMAND_BUTTONS             0x0a

static uint16_t g_encoded_color[2] = {
    GBA_GFX_ENCODE_COLOR(0x0000),
    GBA_GFX_ENCODE_COLOR(0xffff)
};
static uint8_t g_color_current = 1;

static void cmd_optimised_1(const uint8_t *data, uint32_t data_size)
{
    uint8_t cmd = 0;
    uint16_t offset = 0;
    uint16_t color = 0;
    uint8_t color_index = 0;
    while (data_size > 0)
    {
        cmd = *data;
        data++;
        data_size--;
        if ((cmd & 0xc0) < 0xc0 )
        {
            // pixel
            if ( data_size < 1 )
            {
                return;
            }
            offset = ((((uint16_t)cmd) << 8) & 0xff00) | ((((uint16_t)(data[0])) << 0) & 0x00ff);
            data++;
            data_size--;
            color = g_encoded_color[g_color_current];
            gba_gfx_draw_pixel_at_offset(offset, color);
        }
        else
        {
            switch(cmd & 0xf0)
            {
                case (0xc0 | 0 << 4): // set color in current index and increment to next index
                    if ( data_size < 2)
                    {
                        return;
                    }
                    color = ((((uint16_t)data[0]) << 8) & 0xff00) | ((((uint16_t)(data[1])) << 0) & 0x00ff);
                    data += 2;
                    data_size -= 2;
                    g_color_current ^= 0x1;
                    g_encoded_color[g_color_current] = GBA_GFX_ENCODE_COLOR(color);
                    break;

                case (0xc0 | 1 << 4): // color flip
                    g_color_current ^= 0x1;
                    break;

                case (0xc0 | 2 << 4): // fill rect
                    color = g_encoded_color[cmd & 0x1];
                    if ( data_size < 4)
                    {
                        return;
                    }
                    offset = ((((uint16_t)data[1]) << 8) & 0xff00) | ((((uint16_t)(data[2])) << 0) & 0x00ff);
                    gba_gfx_fill_rect(offset % 240, offset / 240, data[0], data[3], GBA_GFX_ENCODE_COLOR(color));
                    data += 4;
                    data_size -= 4;
                    break;

                case (0xc0 | 3 << 4): // fill rect
                    color = g_encoded_color[cmd & 0x1];
                    if ( data_size < 3)
                    {
                        return;
                    }
                    offset = ((((uint16_t)data[1]) << 8) & 0xff00) | ((((uint16_t)(data[2])) << 0) & 0x00ff);
                    if ( (cmd & 0x8) == 0 )
                    {
                        // vline
                        gba_gfx_draw_fast_v_line(offset % 240, offset / 240, data[0], GBA_GFX_ENCODE_COLOR(color));
                    }
                    else
                    {
                        // hline
                        gba_gfx_draw_fast_h_line(offset % 240, offset / 240, data[0], GBA_GFX_ENCODE_COLOR(color));
                    }
                    data += 3;
                    data_size -= 3;
                    break;

                default:
                    // unknown command OR corrupt data, stop now
                    return;
            }
        }
    }
}

static void callback(void *ctx, const void *recd_data, uint32_t recd_size, gba_uart_reply_t reply, void *reply_ctx)
{
    if ( recd_size < 1)
    {
        return;
    }
    uint8_t command = ((const uint8_t*)recd_data)[0];
    const uint8_t *data = &((const uint8_t*)recd_data)[1];
    uint32_t data_size = recd_size - 1;
    int16_t x = 0;
    int16_t y = 0;
    int16_t x2 = 0;
    int16_t y2 = 0;
    int16_t w = 0;
    int16_t h = 0;
    uint16_t color = 0x0000;
    switch(command)
    {
        case GBA_COMMAND_OPTIMISED_1:
            cmd_optimised_1(data, data_size);
            break;

        case GBA_COMMAND_DRAW_PIXEL:
            if ( data_size == 4 )
            {
                x = (int16_t)NTOH_UINT8(data, 0);
                y = (int16_t)NTOH_UINT8(data, 1);
                color = NTOH_UINT16(data, 2);
                gba_gfx_draw_pixel(x, y, color);
            }
            break;

        case GBA_COMMAND_DRAW_FAST_VLINE:
            if ( data_size == 5 )
            {
                x = (int16_t)NTOH_UINT8(data, 0);
                y = (int16_t)NTOH_UINT8(data, 1);
                h = (int16_t)NTOH_UINT8(data, 2);
                color = NTOH_UINT16(data, 3);
                gba_gfx_draw_fast_v_line(x, y, h, color);
            }
            break;

        case GBA_COMMAND_DRAW_FAST_HLINE:
            if ( data_size == 5 )
            {
                x = (int16_t)NTOH_UINT8(data, 0);
                y = (int16_t)NTOH_UINT8(data, 1);
                w = (int16_t)NTOH_UINT8(data, 2);
                color = NTOH_UINT16(data, 3);
                gba_gfx_draw_fast_h_line(x, y, w, color);
            }
            break;

        case GBA_COMMAND_FILL_RECT:
            if ( data_size == 6 )
            {
                x = (int16_t)NTOH_UINT8(data, 0);
                y = (int16_t)NTOH_UINT8(data, 1);
                w = (int16_t)NTOH_UINT8(data, 2);
                h = (int16_t)NTOH_UINT8(data, 3);
                color = NTOH_UINT16(data, 4);
                gba_gfx_fill_rect(x, y, w, h, color);
            }
            break;

        case GBA_COMMAND_FILL_SCREEN:
            if ( data_size == 2 )
            {
                color = NTOH_UINT16(data, 0);
                gba_gfx_fill_screen(color);
            }
            break;

        case GBA_COMMAND_DRAW_LINE:
            if ( data_size == 6 )
            {
                x = (int16_t)NTOH_UINT8(data, 0);
                y = (int16_t)NTOH_UINT8(data, 1);
                x2 = (int16_t)NTOH_UINT8(data, 2);
                y2 = (int16_t)NTOH_UINT8(data, 3);
                color = NTOH_UINT16(data, 4);
                gba_gfx_draw_line(x, y, x2, y2, color);
            }
            break;

        case GBA_COMMAND_DRAW_RECT:
            if ( data_size == 6 )
            {
                x = (int16_t)NTOH_UINT8(data, 0);
                y = (int16_t)NTOH_UINT8(data, 1);
                w = (int16_t)NTOH_UINT8(data, 2);
                h = (int16_t)NTOH_UINT8(data, 3);
                color = NTOH_UINT16(data, 4);
                gba_gfx_draw_rect(x, y, w, h, color);
            }
            break;

        case GBA_COMMAND_NOP:
            break;

        default:
            break;
    }
    scanKeys();
    uint16_t buttons = (uint16_t)keysHeld();
    uint8_t msg[3];
    msg[0] = GBA_COMMAND_BUTTONS;
    msg[1] = (uint8_t)((buttons >> 8) & 0xff);
    msg[2] = (uint8_t)((buttons >> 0) & 0xff);
    reply(reply_ctx, msg, sizeof(msg));
}

int main(void)
{
    irqInit();
    irqEnable(IRQ_VBLANK);

    gba_uart_init(&gba_uart_data, callback, NULL);

    gba_gfx_init();
    gba_gfx_fill_screen(0x0000);

    while (1)
    {
        gba_uart_poll(&gba_uart_data);
    }
}
