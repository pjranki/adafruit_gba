#include "gba_gfx.h"

#include <string.h>
#include <gba.h>

#ifndef min
#define min(a, b) (((a) < (b)) ? (a) : (b))
#endif

#ifndef abs
#define abs(v) (((v) < 0) ? ((v) * -1) : (v))
#endif

#ifndef _swap_int16_t
#define _swap_int16_t(a, b)                                                    \
  {                                                                            \
    int16_t t = a;                                                             \
    a = b;                                                                     \
    b = t;                                                                     \
  }
#endif

#define GBA_GFX_SCREEN_BUFFER                   ((uint16_t*)0x6000000)
#define GBA_GFX_SCREEN_PIXEL_AT_OFFSET(offset)  (GBA_GFX_SCREEN_BUFFER[offset])
#define GBA_GFX_SCREEN_PIXEL(x,y)               (GBA_GFX_SCREEN_BUFFER[((int32_t)(y) * (int32_t)GBA_GFX_SCREEN_WIDTH) + (int32_t)(x)])
#define GBA_GFX_SCREEN_PIXEL_SIZE               (sizeof(uint16_t))
#define GBA_GFX_SCREEN_SIZE                     (GBA_GFX_SCREEN_WIDTH*GBA_GFX_SCREEN_HEIGHT*GBA_GFX_SCREEN_PIXEL_SIZE)

void gba_gfx_init()
{
    SetMode(MODE_3 | BG2_ON);
}

void gba_gfx_draw_pixel(int16_t x, int16_t y, uint16_t color)
{
    color = GBA_GFX_ENCODE_COLOR(color);
    GBA_GFX_SCREEN_PIXEL(x, y) = color;
}

void gba_gfx_draw_pixel_at_offset(uint16_t offset, uint16_t encoded_color)
{
    GBA_GFX_SCREEN_PIXEL_AT_OFFSET(offset) = encoded_color;
}

void gba_gfx_draw_fast_v_line(int16_t x, int16_t y, int16_t h, uint16_t color)
{
    color = GBA_GFX_ENCODE_COLOR(color);
    for(int16_t dy = 0; dy < h; dy++)
    {
        GBA_GFX_SCREEN_PIXEL(x, y + dy) = color;  
    }
}

void gba_gfx_draw_fast_h_line(int16_t x, int16_t y, int16_t w, uint16_t color)
{
    color = GBA_GFX_ENCODE_COLOR(color);
    if ( ((color >> 8) & 0xff) == (color & 0xff) )
    {
        memset(&GBA_GFX_SCREEN_PIXEL(x, y), color & 0xff, w * GBA_GFX_SCREEN_PIXEL_SIZE);
        return;
    }
    for(int16_t dx = 0; dx < w; dx++)
    {
        GBA_GFX_SCREEN_PIXEL(x + dx, y) = color;  
    }
}

void gba_gfx_fill_rect(int16_t x, int16_t y, int16_t w, int16_t h, uint16_t color)
{
    for ( int16_t dy = 0; dy < h; dy++ )
    {
        gba_gfx_draw_fast_h_line(x, y + dy, w, color);
    }
}

void gba_gfx_fill_screen(uint16_t color)
{
    color = GBA_GFX_ENCODE_COLOR(color);
    if ( ((color >> 8) & 0xff) == (color & 0xff) )
    {
        memset(GBA_GFX_SCREEN_BUFFER, color & 0xff, GBA_GFX_SCREEN_SIZE);
        return;
    }
    uint16_t tmp[GBA_GFX_SCREEN_WIDTH];
    for(int16_t x = 0; x < GBA_GFX_SCREEN_WIDTH; x++)
    {
        tmp[x] = color;  
    }
    for (int16_t y = 0; y < GBA_GFX_SCREEN_HEIGHT; y++)
    {
        memcpy(&GBA_GFX_SCREEN_PIXEL(0, y), tmp, GBA_GFX_SCREEN_WIDTH * GBA_GFX_SCREEN_PIXEL_SIZE);
    }
}

void gba_gfx_draw_line(int16_t x0, int16_t y0, int16_t x1, int16_t y1, uint16_t color)
{
  int16_t steep = abs(y1 - y0) > abs(x1 - x0);
  if (steep) {
    _swap_int16_t(x0, y0);
    _swap_int16_t(x1, y1);
  }

  if (x0 > x1) {
    _swap_int16_t(x0, x1);
    _swap_int16_t(y0, y1);
  }

  int16_t dx, dy;
  dx = x1 - x0;
  dy = abs(y1 - y0);

  int16_t err = dx / 2;
  int16_t ystep;

  if (y0 < y1) {
    ystep = 1;
  } else {
    ystep = -1;
  }

  for (; x0 <= x1; x0++) {
    if (steep) {
      gba_gfx_draw_pixel(y0, x0, color);
    } else {
      gba_gfx_draw_pixel(x0, y0, color);
    }
    err -= dy;
    if (err < 0) {
      y0 += ystep;
      err += dx;
    }
  }
}

void gba_gfx_draw_rect(int16_t x, int16_t y, int16_t w, int16_t h, uint16_t color)
{
    gba_gfx_draw_fast_h_line(x, y, w, color);
    gba_gfx_draw_fast_h_line(x, y + h, w, color);
    gba_gfx_draw_fast_v_line(x, y, h, color);
    gba_gfx_draw_fast_v_line(x + w, y, h, color);
}
