#!/bin/bash

export DEVKITPRO=/opt/devkitpro
export DEVKITARM=/opt/devkitpro/devkitARM
export DEVKITPPC=/opt/devkitpro/devkitPPC

mkdir out
cd out
cmake ..
make
echo -n "static const " > Adafruit_GBA_ROM.h
xxd -i Adafruit_GBA_ROM.gba >> Adafruit_GBA_ROM.h
cp Adafruit_GBA_ROM.h ../../Adafruit_GBA_ROM.h
cp Adafruit_GBA_ROM.gba ../../Adafruit_GBA_ROM.gba
cd ..
