#ifndef _H_GBA_MULTIBOOT_H_
#define _H_GBA_MULTIBOOT_H_

#include <stdint.h>
#include <stdbool.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef bool(*gbamb_spi_transaction_t)(void *ctx, void *buffer, uint32_t size);
typedef uint8_t(*gbamb_rom_read_t)(void *ctx, uint32_t offset);

typedef struct gbamb_data {
    void *ctx;
    gbamb_spi_transaction_t transaction;
    uint32_t fcnt;
} gbamb_data_t;

void gbamb_init(gbamb_data_t *gbamb, gbamb_spi_transaction_t transaction, void *ctx);
bool gbamb_load(gbamb_data_t *gbamb, uint32_t rom_size, gbamb_rom_read_t rom_read, void *ctx);

#ifdef __cplusplus
};
#endif

#endif
