#include "Adafruit_GBA.h"

#define GBA_COMMAND_RESERVED            0x00
#define GBA_COMMAND_DRAW_PIXEL          0x01
#define GBA_COMMAND_DRAW_FAST_VLINE     0x02
#define GBA_COMMAND_DRAW_FAST_HLINE     0x03
#define GBA_COMMAND_FILL_RECT           0x04
#define GBA_COMMAND_FILL_SCREEN         0x05
#define GBA_COMMAND_DRAW_LINE           0x06
#define GBA_COMMAND_DRAW_RECT           0x07
#define GBA_COMMAND_OPTIMISED_1         0x08
#define GBA_COMMAND_NOP                 0x09
#define GBA_COMMAND_BUTTONS             0x0a

static uint8_t gba_default_rom_read(void *ctx_, uint32_t offset)
{
    void **ctx = (void**)ctx_;
    uint32_t rom_size = *((uint32_t*)(ctx[0]));
    const uint8_t *rom_data = (const uint8_t *)(ctx[1]);
    if ( offset >= rom_size )
    {
        return 0x00;
    }
    return rom_data[offset];
}

bool Adafruit_GBA::load(gbamb_spi_transaction_t transaction, uint32_t rom_size, const void *rom_data, void *transaction_ctx)
{
    void *ctx[2] = {(void*)&rom_size, (void*)rom_data};
    return this->load(transaction, rom_size, gba_default_rom_read, transaction_ctx, (void*)ctx);
}

bool Adafruit_GBA::load(gbamb_spi_transaction_t transaction, uint32_t rom_size, gbamb_rom_read_t rom_read, void *transaction_ctx, void *rom_read_ctx)
{
    if ( !transaction || !rom_size || !rom_read )
    {
        return false;
    }
    gbamb_data_t gbamb;
    gbamb_init(&gbamb, transaction, transaction_ctx);
    return gbamb_load(&gbamb, rom_size, rom_read, rom_read_ctx);
}

bool Adafruit_GBA::begin(gba_uart_send_t send, gba_uart_recv_t recv, void *ctx)
{
    if ( !send || !recv )
    {
        return false;
    }
    gba_uart_init(&this->gba_uart_, send, recv, ctx);
    delay(6000);
    return true;
}

void Adafruit_GBA::callback(const uint8_t *reply_data, uint32_t reply_size)
{
    if ( (reply_size >= 3) && (reply_data[0] == GBA_COMMAND_BUTTONS ) && button_callback_ )
    {
        uint16_t buttons = (((uint16_t)(reply_data[1])) << 8) & 0xff00 | (((uint16_t)(reply_data[2])) << 0) & 0x00ff;
        if ( buttons != buttons_prev_ )
        {
            button_callback_(buttons);
            buttons_prev_ = buttons;
        }
    }
}

void Adafruit_GBA::poll()
{
    uint8_t reply[20];
    uint8_t msg[1];
    msg[0] = GBA_COMMAND_NOP;
    if ( gba_uart_transaction(&this->gba_uart_, msg, sizeof(msg), reply, sizeof(reply)) )
    {
        callback(reply, sizeof(reply));
    }
}

void Adafruit_GBA::drawPixel(int16_t x, int16_t y, uint16_t color)
{
    uint8_t reply[20];
    uint8_t msg[5];
    msg[0] = GBA_COMMAND_DRAW_PIXEL;
    msg[1] = (uint8_t)x;
    msg[2] = (uint8_t)y;
    msg[3] = (uint8_t)((color >> 8) & 0xff);
    msg[4] = (uint8_t)((color >> 0) & 0xff);
    if ( gba_uart_transaction(&this->gba_uart_, msg, sizeof(msg), reply, sizeof(reply)) )
    {
        callback(reply, sizeof(reply));
    }
}

void Adafruit_GBA::drawFastVLine(int16_t x, int16_t y, int16_t h, uint16_t color)
{
    uint8_t reply[20];
    uint8_t msg[6];
    msg[0] = GBA_COMMAND_DRAW_FAST_VLINE;
    msg[1] = (uint8_t)x;
    msg[2] = (uint8_t)y;
    msg[3] = (uint8_t)h;
    msg[4] = (uint8_t)((color >> 8) & 0xff);
    msg[5] = (uint8_t)((color >> 0) & 0xff);
    if ( gba_uart_transaction(&this->gba_uart_, msg, sizeof(msg), reply, sizeof(reply)) )
    {
        callback(reply, sizeof(reply));
    }
}

void Adafruit_GBA::drawFastHLine(int16_t x, int16_t y, int16_t w, uint16_t color)
{
    uint8_t reply[20];
    uint8_t msg[6];
    msg[0] = GBA_COMMAND_DRAW_FAST_HLINE;
    msg[1] = (uint8_t)x;
    msg[2] = (uint8_t)y;
    msg[3] = (uint8_t)w;
    msg[4] = (uint8_t)((color >> 8) & 0xff);
    msg[5] = (uint8_t)((color >> 0) & 0xff);
    if ( gba_uart_transaction(&this->gba_uart_, msg, sizeof(msg), reply, sizeof(reply)) )
    {
        callback(reply, sizeof(reply));
    }
}

void Adafruit_GBA::fillRect(int16_t x, int16_t y, int16_t w, int16_t h,
                    uint16_t color)
{
    uint8_t reply[20];
    uint8_t msg[7];
    msg[0] = GBA_COMMAND_FILL_RECT;
    msg[1] = (uint8_t)x;
    msg[2] = (uint8_t)y;
    msg[3] = (uint8_t)w;
    msg[4] = (uint8_t)h;
    msg[5] = (uint8_t)((color >> 8) & 0xff);
    msg[6] = (uint8_t)((color >> 0) & 0xff);
    if ( gba_uart_transaction(&this->gba_uart_, msg, sizeof(msg), reply, sizeof(reply)) )
    {
        callback(reply, sizeof(reply));
    }
}

void Adafruit_GBA::fillScreen(uint16_t color)
{
    uint8_t reply[20];
    uint8_t msg[3];
    msg[0] = GBA_COMMAND_FILL_SCREEN;
    msg[1] = (uint8_t)((color >> 8) & 0xff);
    msg[2] = (uint8_t)((color >> 0) & 0xff);
    if ( gba_uart_transaction(&this->gba_uart_, msg, sizeof(msg), reply, sizeof(reply)) )
    {
        callback(reply, sizeof(reply));
    }
}

void Adafruit_GBA::drawLine(int16_t x0, int16_t y0, int16_t x1, int16_t y1,
                        uint16_t color)
{
    uint8_t reply[20];
    uint8_t msg[7];
    msg[0] = GBA_COMMAND_DRAW_LINE;
    msg[1] = (uint8_t)x0;
    msg[2] = (uint8_t)y0;
    msg[3] = (uint8_t)x1;
    msg[4] = (uint8_t)y1;
    msg[5] = (uint8_t)((color >> 8) & 0xff);
    msg[6] = (uint8_t)((color >> 0) & 0xff);
    if ( gba_uart_transaction(&this->gba_uart_, msg, sizeof(msg), reply, sizeof(reply)) )
    {
        callback(reply, sizeof(reply));
    }
}

void Adafruit_GBA::drawRect(int16_t x, int16_t y, int16_t w, int16_t h,
                        uint16_t color)
{
    uint8_t reply[20];
    uint8_t msg[7];
    msg[0] = GBA_COMMAND_DRAW_RECT;
    msg[1] = (uint8_t)x;
    msg[2] = (uint8_t)y;
    msg[3] = (uint8_t)w;
    msg[4] = (uint8_t)h;
    msg[5] = (uint8_t)((color >> 8) & 0xff);
    msg[6] = (uint8_t)((color >> 0) & 0xff);
    if ( gba_uart_transaction(&this->gba_uart_, msg, sizeof(msg), reply, sizeof(reply)) )
    {
        callback(reply, sizeof(reply));
    }
}

uint16_t Adafruit_GBA::color565(uint8_t red, uint8_t green, uint8_t blue)
{
    return ((red & 0xF8) << 8) | ((green & 0xFC) << 3) | (blue >> 3);
}

void Adafruit_GBA::startWrite(void)
{
    msg_buffer_pos_ = 0;
}

void Adafruit_GBA::endWrite(void)
{
    uint8_t reply[20];
    if ( msg_buffer_pos_ > 0 )
    {
        if ( gba_uart_transaction(&this->gba_uart_, msg_buffer_, msg_buffer_pos_, reply, sizeof(reply)) )
        {
            callback(reply, sizeof(reply));
        }
    }
    msg_buffer_pos_ = 0;
}

void Adafruit_GBA::writeCommand(const uint8_t *data, size_t size)
{
    uint8_t reply[20];
    uint8_t msg[20];
    if ( msg_buffer_pos_ + size > sizeof(msg_buffer_) )
    {
        if ( gba_uart_transaction(&this->gba_uart_, msg_buffer_, msg_buffer_pos_, reply, sizeof(reply)) )
        {
            callback(reply, sizeof(reply));
        }
        msg_buffer_pos_ = 0;
    }
    if ( msg_buffer_pos_ == 0 )
    {
        msg_buffer_[msg_buffer_pos_] = GBA_COMMAND_OPTIMISED_1;
        msg_buffer_pos_++;
    }
    memcpy(&msg_buffer_[msg_buffer_pos_], data, size);
    msg_buffer_pos_ += size;
}

uint8_t Adafruit_GBA::writeColorIndex(uint16_t color, bool color_flip_needed)
{
    uint8_t msg[3];
    uint8_t color_index = 0xff;
    for ( uint8_t i = 0; i < 2; i++ )
    {
        if ( this->color_[i] == color )
        {
            color_index = i;
            break;
        }
    }
    if ( color_index == 0xff )
    {
        msg[0] = 0xc0 | 0 << 4; // set color in current index and increment to next index
        msg[1] = (uint8_t)((color >> 8) & 0xff);
        msg[2] = (uint8_t)((color >> 0) & 0xff);
        writeCommand(msg, 3);
        this->color_current_ ^= 0x1;
        this->color_[this->color_current_] = color;
        color_index = this->color_current_;
    }
    if ( this->color_[this->color_current_] != color )
    {
        msg[0] = 0xc0 | 1 << 4; // color flip
        writeCommand(msg, 1);
        this->color_current_ ^= 0x1;
    }
    return this->color_current_;
}

void Adafruit_GBA::writePixel(int16_t x, int16_t y, uint16_t color)
{
    if ( x < 0 || x >= 240 || y < 0 || y >=160 )
    {
        return;
    }
    writeColorIndex(color, true);
    uint16_t offset = ((uint16_t)y * 240) + (uint16_t)x;  // 38,400 [0x9600]
    uint8_t msg[2];
    msg[0] = (uint8_t)((offset >> 8) & 0xff);
    msg[1] = (uint8_t)((offset >> 0) & 0xff);
    writeCommand(msg, 2);
}

void Adafruit_GBA::writeFillRect(int16_t x, int16_t y, int16_t w, int16_t h,
                                 uint16_t color)
{
    // needed by fillRoundRect(), and drawChar()
    uint16_t command = 0xc000 | (0x2 << 12) | (writeColorIndex(color, false) << 8) | (((uint8_t)w) & 0xff);
    uint16_t offset = ((uint16_t)y * 240) + (uint16_t)x;
    uint8_t msg[5];
    msg[0] = (uint8_t)((command >> 8) & 0xff);
    msg[1] = (uint8_t)((command >> 0) & 0xff);
    msg[2] = (uint8_t)((offset >> 8) & 0xff);
    msg[3] = (uint8_t)((offset >> 0) & 0xff);
    msg[4] = (uint8_t)(h & 0xff);
    writeCommand(msg, 5);
}

void Adafruit_GBA::writeFastVLine(int16_t x, int16_t y, int16_t h, uint16_t color)
{
    // needed by fillCircle(), fillCircleHelper(), drawRoundRect(), drawChar()
    uint16_t command = 0xc000 | (0x3 << 12) | (0 << 11) | (writeColorIndex(color, false) << 8) | (((uint8_t)h) & 0xff);
    uint16_t offset = ((uint16_t)y * 240) + (uint16_t)x;
    uint8_t msg[4];
    msg[0] = (uint8_t)((command >> 8) & 0xff);
    msg[1] = (uint8_t)((command >> 0) & 0xff);
    msg[2] = (uint8_t)((offset >> 8) & 0xff);
    msg[3] = (uint8_t)((offset >> 0) & 0xff);
    writeCommand(msg, 4);
}

void Adafruit_GBA::writeFastHLine(int16_t x, int16_t y, int16_t w, uint16_t color)
{
    // needed by drawRoundRect(), fillTriangle()
    uint16_t command = 0xc000 | (0x3 << 12) | (1 << 11) | (writeColorIndex(color, false) << 8) | (((uint8_t)w) & 0xff);
    uint16_t offset = ((uint16_t)y * 240) + (uint16_t)x;
    uint8_t msg[4];
    msg[0] = (uint8_t)((command >> 8) & 0xff);
    msg[1] = (uint8_t)((command >> 0) & 0xff);
    msg[2] = (uint8_t)((offset >> 8) & 0xff);
    msg[3] = (uint8_t)((offset >> 0) & 0xff);
    writeCommand(msg, 4);
}

void Adafruit_GBA::writeLine(int16_t x0, int16_t y0, int16_t x1, int16_t y1,
                           uint16_t color)
{
    // only used if drawLine(), drawFastVline() or drawFastHLine is not implemented
    // so we can just call our implementation of drawLine()
    drawLine(x0, y0, x1, y1, color);
}
