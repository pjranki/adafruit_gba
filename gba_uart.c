#include "gba_uart.h"

#include <string.h>

#include <stdio.h>

void gba_uart_init(gba_uart_data_t *gba_uart, gba_uart_send_t send, gba_uart_recv_t recv, void *ctx)
{
    memset(gba_uart, 0, sizeof(*gba_uart));
    gba_uart->ctx = ctx;
    gba_uart->send = send;
    gba_uart->recv = recv;
}

bool gba_uart_transaction(gba_uart_data_t *gba_uart, const void *send_data, uint32_t send_size, void *recv_data, uint32_t recv_size)
{
    if ( !gba_uart || !gba_uart->send || !gba_uart->recv || !send_data )
    {
        return false;
    }
    if ( send_size > 0xff )
    {
        return false;
    }
    uint8_t header[2];
    header[0] = 0x0d;
    header[1] = (uint8_t)(send_size & 0xff);
    if ( !gba_uart->send(gba_uart->ctx, header, 2) )
    {
        return false;
    }
    if ( !gba_uart->send(gba_uart->ctx, send_data, send_size) )
    {
        return false;
    }
    header[0] = 0x00;
    header[1] = 0x00;
    if ( !gba_uart->recv(gba_uart->ctx, header, 2) )
    {
        return false;
    }
    if ( header[0] != 0x0d )
    {
        return false;
    }
    if ( (uint32_t)(header[1]) > recv_size )
    {
        return false;
    }
    recv_size = (uint32_t)(header[1]);
    if ( !gba_uart->recv(gba_uart->ctx, recv_data, recv_size) )
    {
        return false;
    }
    return true;
}
