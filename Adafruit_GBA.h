#ifndef _H_ADAFRUIT_GBA_H_
#define _H_ADAFRUIT_GBA_H_

#include <Adafruit_GFX.h>

#include <functional>

#include "gba_uart.h"
#include "gba_multiboot.h"

#define GBA_GFX_COLOR_BLACK         0x0000
#define GBA_GFX_COLOR_WHITE         0xffff
#define GBA_GFX_COLOR_RED           0xf800
#define GBA_GFX_COLOR_GREEN         0x07e0
#define GBA_GFX_COLOR_BLUE          0x001f
#define GBA_GFX_COLOR_YELLOW        (GBA_GFX_COLOR_RED|GBA_GFX_COLOR_GREEN)
#define GBA_GFX_COLOR_CYAN          (GBA_GFX_COLOR_BLUE|GBA_GFX_COLOR_GREEN)
#define GBA_GFX_COLOR_MAGENTA       (GBA_GFX_COLOR_RED|GBA_GFX_COLOR_BLUE)

class Adafruit_GBA : public Adafruit_GFX
{
  private:
    uint8_t msg_buffer_[0xff];
    uint16_t msg_buffer_pos_;
    uint16_t color_[2];
    uint8_t color_current_;
    uint16_t buttons_prev_;
    gba_uart_data_t gba_uart_;
    std::function<void(uint32_t)> button_callback_;
  protected:
    void writeCommand(const uint8_t *data, size_t size);
    uint8_t writeColorIndex(uint16_t color, bool color_flip_needed);
    void callback(const uint8_t *reply_data, uint32_t reply_size);
  public:
    enum KEYPAD_BITS {
        kKEY_A           =    (1<<0),    /*!< keypad A button */
        kKEY_B           =    (1<<1),    /*!< keypad B button */
        kKEY_SELECT      =    (1<<2),    /*!< keypad SELECT button */
        kKEY_START       =    (1<<3),    /*!< keypad START button */
        kKEY_RIGHT       =    (1<<4),    /*!< dpad RIGHT */
        kKEY_LEFT        =    (1<<5),    /*!< dpad LEFT */
        kKEY_UP          =    (1<<6),    /*!< dpad UP */
        kKEY_DOWN        =    (1<<7),    /*!< dpad DOWN */
        kKEY_R           =    (1<<8),    /*!< Right shoulder button */
        kKEY_L           =    (1<<9),    /*!< Left shoulder button */
        kKEYIRQ_ENABLE   =    (1<<14),    /*!< Enable keypad interrupt */
        kKEYIRQ_OR       =    (0<<15),    /*!< interrupt logical OR mode */
        kKEYIRQ_AND      =    (1<<15),    /*!< interrupt logical AND mode */
        kDPAD            =    (kKEY_UP | kKEY_DOWN | kKEY_LEFT | kKEY_RIGHT) /*!< mask all dpad buttons */
    };
    Adafruit_GBA() : Adafruit_GFX(240, 160)
    {
        msg_buffer_pos_ = 0;
        color_current_ = 1;
        color_[0] = 0x0000;
        color_[1] = 0xffff;
        button_callback_ = nullptr;
        buttons_prev_ = 0x0000;
    };
    bool load(gbamb_spi_transaction_t transaction, uint32_t rom_size, const void *rom_data, void *transaction_ctx = nullptr);
    bool load(gbamb_spi_transaction_t transaction, uint32_t rom_size, gbamb_rom_read_t rom_read, void *transaction_ctx = nullptr, void *rom_read_ctx = nullptr);
    bool begin(gba_uart_send_t send, gba_uart_recv_t recv, void *ctx = nullptr);
    void set_button_callback(std::function<void(uint32_t)> func) { button_callback_ = func; }
    void poll();
    virtual void startWrite(void);
    virtual void writePixel(int16_t x, int16_t y, uint16_t color);
    virtual void writeFillRect(int16_t x, int16_t y, int16_t w, int16_t h,
                               uint16_t color);
    virtual void writeFastVLine(int16_t x, int16_t y, int16_t h, uint16_t color);
    virtual void writeFastHLine(int16_t x, int16_t y, int16_t w, uint16_t color);
    virtual void writeLine(int16_t x0, int16_t y0, int16_t x1, int16_t y1,
                           uint16_t color);
    virtual void endWrite(void);
    virtual void drawPixel(int16_t x, int16_t y, uint16_t color);
    virtual void drawFastVLine(int16_t x, int16_t y, int16_t h, uint16_t color);
    virtual void drawFastHLine(int16_t x, int16_t y, int16_t w, uint16_t color);
    virtual void fillRect(int16_t x, int16_t y, int16_t w, int16_t h,
                        uint16_t color);
    virtual void fillScreen(uint16_t color);
    virtual void drawLine(int16_t x0, int16_t y0, int16_t x1, int16_t y1,
                          uint16_t color);
    virtual void drawRect(int16_t x, int16_t y, int16_t w, int16_t h,
                          uint16_t color);
    uint16_t color565(uint8_t red, uint8_t green, uint8_t blue);
};

#endif
